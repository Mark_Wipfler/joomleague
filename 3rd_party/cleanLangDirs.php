<?php
/**
 * @author 		Wolfgang Pinitsch <andone@mfga.at>
 * @package	 	Joomla
 * @subpackage  Joomleague language files administration
 * @copyright	Copyright (C) 2005-2014 joomleague.at. All rights reserved.
 * @license	 	GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

$username = null;
$password = null;

if(!isset($argv[1]) && !isset($argv[2]) ) {
	echo 'transifex username and password needed'.PHP_EOL;
	echo ''.PHP_EOL;
	echo 'cleanLangDirs.php <tx username> <tx password>'.PHP_EOL;
	exit -1;
}
if(!isset($argv[1])) {
	echo 'transifex username needed'.PHP_EOL;
	echo ''.PHP_EOL;
	echo 'cleanLangDirs.php <tx username> <tx password>'.PHP_EOL;
	exit -1; 
} else {
	$username = $argv[1];
}
if(!isset($argv[2])) {
	echo 'transifex user password needed'.PHP_EOL;
	echo ''.PHP_EOL;
	echo 'cleanLangDirs.php <tx username> <tx password>'.PHP_EOL;
	exit -1;
} else {
	$password = $argv[2];
}

function getTotalSize($dir)
{
	$dir = rtrim(str_replace('\\', '/', $dir), '/');

	if (is_dir($dir) === true) {
		$totalSize = 0;
		$os        = strtoupper(substr(PHP_OS, 0, 3));
		// If on a Unix Host (Linux, Mac OS)
		if ($os !== 'WIN') {
			$io = popen('/usr/bin/du -sb ' . $dir, 'r');
			if ($io !== false) {
				$totalSize = intval(fgets($io, 80));
				pclose($io);
				return $totalSize;
			}
		}
		// If on a Windows Host (WIN32, WINNT, Windows)
		if ($os === 'WIN' && extension_loaded('com_dotnet')) {
			$obj = new \COM('scripting.filesystemobject');
			if (is_object($obj)) {
				$ref       = $obj->getfolder($dir);
				$totalSize = $ref->size;
				$obj       = null;
				return $totalSize;
			}
		}
		// If System calls did't work, use slower PHP 5
		$files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir));
		foreach ($files as $file) {
			$totalSize += $file->getSize();
		}
		return $totalSize;
	} else if (is_file($dir) === true) {
		return filesize($dir);
	}
}

function formatSize($filesize, $unit='m') {
	switch ($unit) {
		case 'g': $filesize = number_format($filesize / 1073741824, 3); break;  // giga
		case 'm': $filesize = number_format($filesize / 1048576, 1);    break;  // mega
		case 'k': $filesize = number_format($filesize / 1024, 0);       break;  // kilo
		case 'b': $filesize = number_format($filesize, 0);              break;  // byte
	}
	return $filesize . $unit;
}

function delete_files($target) {
	global $iDeleted;
	if(is_dir($target)){
		$files = glob( $target . '/*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
		echo "checking ". count($files)." files".PHP_EOL;
		foreach( $files as $file )
		{
			delete_files( $file );
		}
		rmdir( $target );
		echo "deleting " .$target.PHP_EOL;
	} elseif(is_file($target)) {
		@chmod($target, 0777);
		echo "deleting " .$target.PHP_EOL;
		$iDeleted++;
		unlink( $target );
	}
}

$lines = file ( 'transifex/config' );

echo 'Current Directory Size: '.formatSize(getTotalSize(__DIR__ . '/../')).PHP_EOL;
foreach ( $lines as $line_num => $line ) {
	$res = '';
	if (substr ( $line, 0, 2 ) == '[j') {
		$path = __DIR__ . '/../' . $lines [$line_num + 2];
		// source_file = com_joomleague/administrator/components/com_joomleague/language/en-GB/en-GB.com_joomleague_sport_types.ini
		$path = str_replace ( 'source_file = ', '', $path );
		$path = str_replace ( 'en-GB', '', $path );
		$path = substr ( $path, 0, strrpos ( $path, '/' ) );
		$path = str_replace('\\', '/', $path);
		$pattern = '/\[joomleague\.(.*?)\]/';
		preg_match ( $pattern, $line, $matches );
		$resource = $matches [1];
		echo 'working on ' . $resource . '...'.PHP_EOL;
		$url = 'http://'.$username.':'.$password.'@www.transifex.com/api/2/project/joomleague/resource/' . $resource . '/stats/';
		$response = file_get_contents ( $url );
		$objResponse = json_decode ( $response );
		foreach ( $objResponse as $key => $value ) {
			$iDeleted = 0;
			$completed = $objResponse->{"$key"}->completed;
			$dir = $path . str_replace('_','-',$key);
			//if(!is_dir($dir)){
			//	mkdir($dir);
			//}
			if ($completed == '0%' && ($key != 'en_GB') && is_dir($dir)) {
				echo "working on ".$dir.' ' .$completed.PHP_EOL;
				delete_files ( $dir );
				echo "deleted " .$iDeleted. " files".PHP_EOL;
			} else {
				echo 'skipped ' .$dir.' due '.$completed. ' and found: ' . (is_dir($dir) ? 'true' : 'false') . PHP_EOL;
			}
		}
		echo 'done!'.PHP_EOL;
	}
}

echo 'New Directory Size: '.formatSize(getTotalSize(__DIR__ . '/../')).PHP_EOL;

